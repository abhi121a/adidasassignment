package Pages;

import Base.BaseClass;
import Utils.WaitUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CartPage extends BaseClass {
//Variables
    public String cartAmount;

    static Logger logger = Logger.getLogger(HomePage.class);


 //Constructor
    public CartPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 20), this);
    }

// WebElements
    @FindBy (xpath="//div[@class='table-responsive']//td[contains(text(),'Dell i7 8gb')]/ancestor::tr//a[contains(text(),'Delete')]")
    WebElement deleteDellLaptop;
    @FindBy(xpath="//button[@class='btn btn-success' and contains(text(),'Place Order')]")
    WebElement placeOrder;
    @FindBy(xpath="//h3[@id='totalp']")
    WebElement totalCartAmount;
    @FindBy(xpath="//div[@class='table-responsive']//td")
    WebElement carttable;

    public void clickDeleteLaptop(){
        deleteDellLaptop.click();
        logger.info("Deleted Dell Laptop");

    }
    public void clickDeleteLaptop(String name){
        driver.findElement(By.xpath("//div[@class='table-responsive']//td[contains(text(),'"+name+"')]/ancestor::tr//a[contains(text(),'Delete')]")).click();
        logger.info("Deleted product::"+name);
    }

    public PlaceOrder clickPlaceOrder() throws Exception{
        logger.info("clicking Place Order");
        wait.until(ExpectedConditions.visibilityOf(carttable));
        WaitUtils.waitForLoad(driver);
        placeOrder.click();
        logger.info("Place Order CLicked");
        return new PlaceOrder(driver);

    }

    public void getCartAmount() throws Exception{
        wait.until(ExpectedConditions.visibilityOfAllElements(carttable));
        WaitUtils.waitForLoad(driver);
      cartAmount=totalCartAmount.getText();
      logger.info("CartAmount::"+cartAmount);
    }


}
