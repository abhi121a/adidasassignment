package Pages;

import Base.BaseClass;
import Utils.WaitUtils;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage extends BaseClass {
    //Variables
    static Logger logger = Logger.getLogger(HomePage.class);

    //Web Elements
    @FindBy(xpath = "//a[contains(text(),'Phones')]")
    private WebElement phone;
    @FindBy(xpath = "//a[contains(text(),'Laptops')]")
    private WebElement laptop;
    @FindBy(xpath = "//a[contains(text(),'Monitors')]")
    private WebElement monitor;
    //Dynamic xpath i.e parameter driven . we added special character on it
    @FindBy(xpath = "//a[contains(text(),'$$$')]")
    private WebElement productTypeElement;

    @FindBy(xpath = "//*[@class='hrefch' and contains(text(),'Samsung galaxy s6')]")
    private WebElement samsungMobile;
    @FindBy(xpath = "//*[@class='hrefch' and contains(text(),'Sony vaio i5')]")
    private WebElement sonyLaptop;
    @FindBy(xpath = "//*[@class='hrefch' and contains(text(),'Dell i7 8gb')]")
    private WebElement dellLaptop;
    @FindBy(xpath = "//*[@class='hrefch' and contains(text(),'Apple monitor 24')]")
    private WebElement appleMonitor;
    @FindBy(xpath = "//*[@class='hrefch' and contains(text(),'$$$')]")
    private WebElement productNameElement;
    @FindBy(xpath = "//*[@id=\"cartur\"]")
    private WebElement cart;

    //Contructor
    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 20), this);
    }

    //Action Methods
    public void clickPhoneOnHomePage() {
        phone.click();
    }

    public void clickLaptopOnHomePage() {
        laptop.click();
    }

    public void clickMonitorOnHomePage() {
        monitor.click();
    }

    public void clicksonyLaptop() {
        sonyLaptop.click();
    }

    public void clickDellLaptop() {
        dellLaptop.click();
    }

    public CartPage clickCart() {
        cart.click();
        return new CartPage(driver);
    }

    public void navigateAllTabs() {
        logger.info("Clicking on Phone");
        clickPhoneOnHomePage();
        WaitUtils.waitForLoad(driver);
        wait.until(ExpectedConditions.visibilityOf(samsungMobile));
        Assert.assertTrue(samsungMobile.isDisplayed());
        logger.info("Verifed mobile is displayed");
        logger.info("Clicking on Laptop");
        clickLaptopOnHomePage();
        WaitUtils.waitForLoad(driver);
        wait.until(ExpectedConditions.visibilityOf(sonyLaptop));
        Assert.assertTrue(sonyLaptop.isDisplayed());
        logger.info("Verifying laptop is displayed");
        logger.info("Clicking on Monitor");
        clickMonitorOnHomePage();
        WaitUtils.waitForLoad(driver);
        wait.until(ExpectedConditions.visibilityOf(appleMonitor));
        logger.info("Verifying Monitor is displayed");
        Assert.assertTrue(appleMonitor.isDisplayed());
    }

    public PDPPage clickProduct(String productType, String productName) throws Exception {

        //dynamicXpathUpdater(productTypeElement,productType).click();
        logger.info("CLick laptop on HOmePage");
        driver.findElement(By.xpath("//a[contains(text(),'" + productType + "')]")).click();
        logger.info("Verifying laptop is displayed");
        // dynamicXpathUpdater(productNameElement,product).click();
        logger.info("Clicking Produt on HomePage");
        String productNameXpath = "//a[contains(text(),'" + productName + "')]";
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(productNameXpath)));
        WaitUtils.waitForLoad(driver);
        driver.findElement(By.xpath(productNameXpath)).click();
        return new PDPPage(driver);
    }

    //below method is taking longer in updating the xpath hence updating it in different way
    public WebElement dynamicXpathUpdater(WebElement element, String arg) {
        logger.info("Inside Dynamic xpath Updater");
        logger.info("Element to be updated with arg::" + arg);
        String elementXpath = productTypeElement.toString().split("xpath:")[1];
        String newxpath = elementXpath.substring(0, elementXpath.lastIndexOf("'")).trim().replace("$$$", arg);
        logger.info("Updated xpath::" + newxpath);
        return driver.findElement(By.xpath(newxpath));
    }


}
