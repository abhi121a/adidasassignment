package Pages;

import Base.BaseClass;
import Utils.WaitUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PDPPage extends BaseClass {
    //Variables
    static Logger logger = Logger.getLogger(PDPPage.class);

    //Web Elements
    @FindBy(xpath = "//a[contains(text(),'Add to cart')]")
    private WebElement addtoCart;

    //Constructor
    public PDPPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 8), this);
    }

    //Methods
    public void clickAddToCart() {
        addtoCart.click();
    }

    public void addproductTocartAndCLikOk() {
        WaitUtils.waitForLoad(driver);
        clickAddToCart();
        wait.until(ExpectedConditions.alertIsPresent());
        driver.switchTo().alert().accept();
    }
}
