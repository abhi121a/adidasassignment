package Pages;

import Base.BaseClass;
import Utils.WaitUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PlaceOrder extends BaseClass {
    //Variables
    public String id;
    public String amount;
    static Logger logger = Logger.getLogger(PlaceOrder.class);


    //Contructor
    public PlaceOrder(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 20), this);
    }

    //WebElements

    @FindBy(xpath="//input[@id='name']")
    WebElement name;
    @FindBy(xpath="//input[@id='country']")
    WebElement country;
    @FindBy(xpath="//input[@id='city']")
    WebElement city;
    @FindBy(xpath="//input[@id='card']")
    WebElement card;
    @FindBy(xpath="//input[@id='month']")
    WebElement month;
    @FindBy(xpath="//input[@id='year']")
    WebElement year;
    @FindBy(xpath="//button[contains(text(),'Purchase')]")
    WebElement purchase;
    @FindBy(id="orderModal")
    WebElement placeOrderFormModal;

    @FindBy(xpath="//div[@class='sweet-alert  showSweetAlert visible']/p")
    WebElement purchaseRecipt;

    @FindBy(xpath = "//button[@class='confirm btn btn-lg btn-primary' and contains(text(),'OK')]")
    WebElement okbutton;

//Methods
public void fillForm() throws Exception{
        WaitUtils.waitForLoad(driver);
        name.sendKeys("Abhishek");
        country.sendKeys("India");
        city.sendKeys("Gurugram");
        card.sendKeys("1212121");
        month.sendKeys("10");
        year.sendKeys("2020");
    }
    public void clickPurchase(){
      purchase.click();
    }
    public void getTextfromPurchaseRecipt(){
      String purchaseReceipt=purchaseRecipt.getText();
      logger.info("PurchaseReceipt::"+purchaseReceipt);
      purchaseReciptTextProcessor(purchaseReceipt);
  }
    public void clickokButton(){
      okbutton.click();
    }


    public void purchaseReciptTextProcessor(String receipt) {
         id = receipt.split("\n")[0].replace("Id:", "");
         amount = receipt.split("\n")[1].replace("Amount:", "").replace("USD","").trim();
    }


   /* public static void main(String args[]){
      String a="Id: 2257877\n" +
              "Amount: 790 USD\n" +
              "Card Number: 1212121\n" +
              "Name: Abhishek\n" +
              "Date: 26/9/2020";
        String id =a.split("\n")[0].replace("Id:","");
        String Amount = a.split("\n")[1].replace("Amount:", "");
        System.out.println(id+"::"+Amount);
    }*/
}
