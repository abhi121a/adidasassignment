package Runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import cucumber.api.testng.CucumberFeatureWrapper;
import org.junit.runner.RunWith;
//Glue  for the specifying the path to the step definitions
@CucumberOptions(
        features = {"src/test/resources"},
        glue = {"stepdefinition"}
)
public class TestRunner extends AbstractTestNGCucumberTests {
}
