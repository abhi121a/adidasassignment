package stepdefinition;


import Base.APITestData;
import Base.BaseClass;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import gherkin.deps.com.google.gson.JsonObject;
import io.restassured.RestAssured;
import io.restassured.internal.path.json.mapping.JsonObjectDeserializer;
import io.restassured.response.ExtractableResponse;
import io.restassured.response.Response;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author abhishek.verma05
 * @Description: Definition class for pet Api
 */

public class ApiDefinitions extends BaseClass {
    String methodName;
    Response response;
    String requestBody;
    HashMap<String,String> requestParameter;
    HashMap<String, String> requestHeader;
    JSONObject requestParams;
    static long id;
    Response  soldresponse;


    static Logger logger = Logger.getLogger(ApiDefinitions.class);
    @Given("^I want to execute findByStatus pet Api$")
    public void i_want_to_execute_findByStatus_pet_Api() throws Exception {
        methodName=new Object() {
        }.getClass().getEnclosingMethod().getName();
        logger.info("Executing Step:"+methodName);
        logger.info("Initialize Base Url");
        RestAssured.basePath="/v2/pet/findByStatus";
        logger.info("Initialize Completed");
        requestParameter=new HashMap<String,String>();
        requestParameter.put("status","available");
    }

    @When("^I Submit the Get request$")
    public void i_Submit_the_Get_request() throws Exception {
        methodName=new Object() {
        }.getClass().getEnclosingMethod().getName();
        logger.info("Executing Step:"+methodName);
        logger.info("Submitting URI");
        response=RestAssured.given().queryParams(requestParameter).get();
    }

    @Then("^I should get (\\d+) response code and i validate it with expected result$")
    public void i_should_get_response_code_and_i_validate_it_with_expected_result(int arg1) throws Exception {
        methodName=new Object() {
        }.getClass().getEnclosingMethod().getName();
        logger.info("Executing Step:"+methodName);
       // logger.info("Verifying response::"+response.body().prettyPrint());
        List<String> allstatus=response.then().statusCode(arg1).extract().body().jsonPath().get("status");
        for(String status:allstatus)
        {
            Assert.assertEquals(status,"available");

        }
           }

    @Given("^I create details for a new Available pet$")
    public void i_get_details_for_the_above_new_Available_pet() throws Exception {
        methodName=new Object() {
        }.getClass().getEnclosingMethod().getName();
        logger.info("Executing Step:"+methodName);
        RestAssured.basePath="/v2/pet";
        requestHeader= new HashMap<String, String>();
        requestHeader.put("Content-Type","application/json");
        requestBody=APITestData.newPedDetailJson;


    }

    @When("^I post the details of new pet$")
    public void I_post_the_details_of_new_pet() throws Exception {
        methodName=new Object() {
        }.getClass().getEnclosingMethod().getName();
        logger.info("Executing Step:"+methodName);
        response =RestAssured.given().headers(requestHeader).body(requestBody).post().then().statusCode(200).extract().response();
        logger.info("Response::"+response.asString());
    }

    @Then("^I assert new pet is added$")
    public void i_assert_new_pet_is_added() throws Exception {
        methodName=new Object() {
        }.getClass().getEnclosingMethod().getName();
        logger.info("Executing Step:"+methodName);
        id=response.then().statusCode(200).extract().body().jsonPath().get("id");
        logger.info("Created id::"+id);


    }


    @When("^I update the pet status to Sold$")
    public void i_update_the_pet_status_to_Sold() throws Exception {
        methodName=new Object() {
        }.getClass().getEnclosingMethod().getName();
        logger.info("Executing Step:"+methodName);
        //hitting the sold api for the above api
        //updating parameters with name and status
        logger.info("Added pet name and status to sold to params");
        requestParameter=new HashMap<String, String>();
        requestHeader=new HashMap<String, String>();
        requestHeader.put("accept","application/json");
        requestHeader.put("Content-Type","application/x-www-form-urlencoded");
        requestParameter.put("name","doggieTest");
        requestParameter.put("status","sold");

        response=RestAssured.given().headers(requestHeader).queryParams(requestParameter).post();
    logger.info("Sold api response ::"+response.asString());
    }

    @Then("^I assert status in updated$")
    public void I_assert_status_in_updated() throws Exception {
        methodName=new Object() {
        }.getClass().getEnclosingMethod().getName();
        logger.info("Executing Step:"+methodName);
      String message=response.then().statusCode(200).extract().body().jsonPath().get("message");
      logger.info("Sold response message::"+message);
       Assert.assertEquals(Long.parseLong(message),id);
    }

    @Given("^I get details for the above new Pet$")
    public void i_get_details_for_the_above_new_Pet() throws Exception {
        methodName=new Object() {
        }.getClass().getEnclosingMethod().getName();
        logger.info("Executing Step:"+methodName);
        //using id from the last automation
        logger.info("Id to be used::"+id);
        RestAssured.basePath="/v2/pet/"+id;
    }

    @When("^I delete the pet$")
    public void i_delete_the_pet() throws Exception {
        methodName=new Object() {
        }.getClass().getEnclosingMethod().getName();
        logger.info("Executing Step:"+methodName);
        requestHeader =new HashMap<String, String>();
        requestParameter=new HashMap<String, String>();
        requestHeader.put("accept","application/json");
        requestHeader.put("api_key","petApiKeyAdidas");
        response=RestAssured.given().headers(requestHeader).delete();
    }

    @Then("^I assert detetion is completed$")
    public void I_assert_detetion_is_completed() throws Exception {
        methodName=new Object() {
        }.getClass().getEnclosingMethod().getName();
        logger.info("Executing Step:"+methodName);
        String message=response.then().statusCode(200).extract().body().jsonPath().get("message");
        logger.info("Deleted response message::"+message);
        Assert.assertEquals(Long.parseLong(message),id);
    }



}
