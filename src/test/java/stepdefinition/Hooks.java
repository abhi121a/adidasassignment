package stepdefinition;

import Base.BaseClass;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.restassured.RestAssured;
import org.apache.commons.lang3.SystemUtils;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.WebDriverWait;


public class Hooks extends BaseClass {
     @Before
    public void setup() {
        PropertyConfigurator.configure("log4j.properties");
        RestAssured.baseURI = baseApiURL;
        String browser = "chrome";
        if (browser.equals("chrome")) {
            if (SystemUtils.IS_OS_WINDOWS_7) {
                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver();
            } else if (SystemUtils.IS_OS_MAC) {
                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver();
            } else if (SystemUtils.IS_OS_LINUX) {
                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver();
            } else {

                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver();
            }
        } else if (browser.equals("firefox")) {
            WebDriverManager.firefoxdriver().setup();
            driver = new ChromeDriver();
        }
    }


    @After
    public void close() {
        if (driver != null) {
            driver.close();
            driver.quit();
        }
    }

}
