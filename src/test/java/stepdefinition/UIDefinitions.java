package stepdefinition;

import Base.BaseClass;
import Pages.CartPage;
import Pages.HomePage;
import Pages.PDPPage;
import Pages.PlaceOrder;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

public class UIDefinitions extends BaseClass {
    HomePage homePage;
    PDPPage pdpPage;
    CartPage cartPage;
    PlaceOrder placeOrder;


    static Logger logger = Logger.getLogger(UIDefinitions.class);
    @Given("^I Open DemoBlaze website$")
    public void i_Open_DemoBlaze_website() throws Exception {
        methodName=new Object() {
        }.getClass().getEnclosingMethod().getName();
        logger.info("Executing Step:"+methodName);
        driver.get(baseUIURL);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        wait=new WebDriverWait(driver,30);
    }

    @When("^I navigation through product categories: Phones, Laptops and Monitors$")
    public void i_navigation_through_product_categories_Phones_Laptops_and_Monitors() throws Exception {
        homePage=new HomePage(driver);
        //navigating to all tabs and validating the products are visible in each
        homePage.navigateAllTabs();
    }

    @When("^Navigate to \"([^\"]*)\" and click on \"([^\"]*)\" and click on \"([^\"]*)\" and Accept popUp$")
    public void navigate_to_and_click_on_and_click_on_and_Accept_popUp(String arg1, String arg2, String arg3) throws Exception {
        // Write code here that turns the phrase above into concrete actions
        String home=driver.getCurrentUrl();
        homePage.clickLaptopOnHomePage();
        pdpPage=homePage.clickProduct(arg1,arg2);
        pdpPage.addproductTocartAndCLikOk();
        driver.get(home);
        homePage=new HomePage(driver);
    }

    @When("^Navigate to Cart and deleted \"([^\"]*)\"$")
    public void navigate_to_Cart_and_deleted(String arg1) throws Exception {
        cartPage = homePage.clickCart();
        cartPage.clickDeleteLaptop(arg1);
        cartPage.getCartAmount();

    }

    @When("^I click on \"([^\"]*)\"$")
    public void i_click_on(String arg1) throws Exception {
       placeOrder= cartPage.clickPlaceOrder();
    }

    @Given("^I fill up the Form and click on purchase$")
    public void i_fill_up_the_Form_and_click_on_purchase() throws Exception {
        placeOrder.fillForm();
        placeOrder.clickPurchase();
    }

    @Then("^I capture and log purchase id and amount$")
    public void i_capture_and_log_purchase_id_and_amount() throws Exception {
        placeOrder.getTextfromPurchaseRecipt();
       logger.info("ID is::"+placeOrder.id);
       logger.info("Amount is"+placeOrder.amount);
    }

    @Then("^I assert purchase amount equals expected$")
    public void i_assert_purchase_amount_equals_expected() throws Exception {
        Assert.assertEquals(placeOrder.amount,cartPage.cartAmount);
    }

    @Then("^Clicked ok$")
    public void clicked_ok() throws Exception {
        placeOrder.clickokButton();
    }
}
