Feature: To Verify Get Available pet Api
Scenario:  Verify Rest Service  Get Available pet
    Given I want to execute findByStatus pet Api
    When I Submit the Get request
    Then I should get 200 response code and i validate it with expected result


Scenario:  POST a new Available pet to the Store. Assert new Pet added
    Given I create details for a new Available pet
    When I post the details of new pet
    Then I assert new pet is added


Scenario: Update this pet status to "sold". Assert status updated.
    Given I get details for the above new Pet
    When I update the pet status to Sold
    Then I assert status in updated

Scenario: Delete the pet and Assert its deleted
    Given  I get details for the above new Pet
    When I delete the pet
    Then I assert detetion is completed