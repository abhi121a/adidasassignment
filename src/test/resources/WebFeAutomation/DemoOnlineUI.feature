Feature: To Verify DemoBlaze UI
  Scenario:  Verify Rest Service  Get Available pet
    Given I Open DemoBlaze website
    When I navigation through product categories: Phones, Laptops and Monitors
    And Navigate to "Laptop" and click on "Sony vaio i5" and click on "Add to cart" and Accept popUp
    And Navigate to "Laptop" and click on "Dell i7 8gb" and click on "Add to cart" and Accept popUp
    And Navigate to Cart and deleted "Dell i7 8gb"
    And I click on "Place order"
    Given I fill up the Form and click on purchase
    Then I capture and log purchase id and amount
    And I assert purchase amount equals expected
    And Clicked ok



